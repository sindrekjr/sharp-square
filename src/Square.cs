using System;

namespace sharp_square
{
    class Square
    {
        int height;
        int width;

        public Square(int height = 10, int width = 10) 
        {
          this.height = height; 
          this.width = width;
          this.render();
          
        }

        void render() {
          for(int i = 0; i < this.height; i++) {
            string output = "|";

            for(int j = 1; j < this.width; j++) {
              output += (i == 0 || i == this.height - 1) ? '-' : ' ';
            }

            output += '|';

            Console.WriteLine(output);
          }
        }
    }
}